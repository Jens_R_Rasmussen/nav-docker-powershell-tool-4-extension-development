if (-not ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {   
    $arguments = "& '" + $myinvocation.mycommand.definition + "'"
    Start-Process powershell -Verb runAs -ArgumentList $arguments
    Break
}
$Settings = Get-Content -Path (Join-Path $PSScriptRoot Settings.json) | ConvertFrom-Json
$Session = New-PSSession -ContainerId $Settings.ContainerId -RunAsAdministrator

$Services = Invoke-Command -Session $Session -ScriptBlock {
return Get-Service 
}

$Service = $services | Out-GridView -OutputMode single -Title "Select Service"
if ($null -eq $Service) {
    Remove-PSSession $Session
    Write-Host -ForegroundColor Green "Done"
    Start-Sleep -Seconds 2
    return
}
if ($Service.Status -eq 'Running') {
    $StopService = Read-Host -Prompt "Do you realy want to stop service $($Service.ServiceName) (Y/N)"
    if ($StopService.ToUpper() -ne 'Y') {
        Remove-PSSession $Session
        Write-Host -ForegroundColor Green "Done"
        Start-Sleep -Seconds 2
        return
    }
    Invoke-Command -Session $Session -ScriptBlock {
        param($ServiceName)
        Write-Host -ForegroundColor Green "Stopping: $ServiceName"
        $Service = Get-Service $ServiceName
        $Service.Stop()    
    } -ArgumentList $Service.ServiceName
}
else {
    Invoke-Command -Session $Session -ScriptBlock {
        param($ServiceName)
        Write-Host -ForegroundColor Green "Starting: $ServiceName"
        $Service = Get-Service $ServiceName
        $Service.Start()    
    } -ArgumentList $Service.ServiceName
}
Remove-PSSession $Session
Write-Host -ForegroundColor Green "Done"
Start-Sleep -Seconds 2