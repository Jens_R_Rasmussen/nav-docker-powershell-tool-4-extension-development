if (-not ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {   
    $arguments = "& '" + $myinvocation.mycommand.definition + "'"
    Start-Process powershell -Verb runAs -ArgumentList $arguments
    Break
}
$Settings = Get-Content -Path (Join-Path $PSScriptRoot Settings.json) | ConvertFrom-Json
$Session = New-PSSession -ContainerId $Settings.ContainerId -RunAsAdministrator
Invoke-Command -Session $Session -ScriptBlock {
    Import-Module 'C:\Program Files\Microsoft Dynamics NAV\*\Service\NavAdminTool.ps1' | Out-Null
    $ServiceInstance = "NAV"
    $CurrentServiceInstance = Get-NAVServerInstance -ServerInstance $ServiceInstance
    $Tenantes = $CurrentServiceInstance | Get-NAVTenant | Select-Object Id
    foreach ($Tenant in $Tenantes) {
        Write-Host "Sync Tenant: $($Tenant.Id)"
        $CurrentServiceInstance | Sync-NAVTenant -Tenant $Tenant.Id -Mode ForceSync -Force   
    }
}
Remove-PSSession $Session
Write-Host
Write-Host -ForegroundColor Green "Done!"
Start-Sleep -Seconds 2
