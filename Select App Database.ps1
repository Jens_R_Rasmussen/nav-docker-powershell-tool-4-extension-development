if (-not ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {   
    $arguments = "& '" + $myinvocation.mycommand.definition + "'"
    Start-Process powershell -Verb runAs -ArgumentList $arguments
    Break
}
$Settings = Get-Content -Path (Join-Path $PSScriptRoot Settings.json) | ConvertFrom-Json
$Session = New-PSSession -ContainerId $Settings.ContainerId -RunAsAdministrator
$Databases = Invoke-Command -Session $Session -ScriptBlock {
    $Module = "SqlServer"
    Clear-Host
    $ModuleExist = Get-Module -Name $Module -ListAvailable
    if (!($ModuleExist)) {
        Write-Host -ForegroundColor Green "Install $Module (Y/N)" -NoNewline
        $Install = (Read-host).ToUpper()
        if ($Install -eq 'Y') {
            Write-Host
            Write-Host -ForegroundColor Green "Installing $Module"
            Install-Module -Name $Module -Force
            Import-Module -Name $Module
        }
    }
    else {
        Import-Module -Name $Module
    }
    $Databases = Invoke-Sqlcmd -Query "SELECT name FROM sys.databases WHERE name not in ('master', 'tempdb', 'model', 'msdb')" -Database master
    return $Databases
}
Remove-PSSession $Session

$Database = $Databases | Out-GridView -OutputMode single -Title "Please select Database"
if ($null -ne $Database) {
    Write-Host 
    Write-Host -ForegroundColor Green "Updating Settings"
    $Settings.DatabaseName = $Database.name
    Set-Content -Value ($Settings | ConvertTo-json) -Path (Join-Path $PSScriptRoot Settings.json)
}
Write-Host 
Write-Host -ForegroundColor Green "Done"
Start-Sleep -Seconds 2