if (-not ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {   
    $arguments = "& '" + $myinvocation.mycommand.definition + "'"
    Start-Process powershell -Verb runAs -ArgumentList $arguments
    Break
}
Clear-Host
$Object = @()
[switch]$StartContainer = $false
docker ps -a --no-trunc --format '{{.Names}};{{.ID}};{{.Status}};{{.Labels}};{{.Image}};{{.Mounts}}' | ForEach-Object { 
    $Items = $_.split(';');
    $Row = @{Name = "$($Items.Item(0))"; ID = "$($Items.Item(1))"; Status = "$($Items.Item(2))"; Image = "$($Items.Item(4))"}
    $Items.Item(3).split(',') | ForEach-Object {
        $Row.Add($_.Substring(0, 1).ToUpper() + $_.Substring(1, $_.IndexOf('=') - 1), $_.Substring($_.IndexOf('=') + 1))
    }
    $Items.Item(5).split(',') | ForEach-Object {
        if ($_ -like '*client')
        {    
            $Row.Add('Path', $_)
        }
    }
    $Object += $Row
}
$Object = $Object | ForEach-Object { New-Object object | Add-Member -NotePropertyMembers $_ -PassThru }

$NavContainers = $Object | Where-Object {$_.Nav -ne $null}

if ($NavContainers -eq $null) {
    Write-Host -ForegroundColor Red "There is no NAV Container on this machine"
    Start-Sleep -Seconds 2
    return
}

$Container = $NavContainers | Select-Object Name, ID, Nav, Cu, Version, Path | Out-GridView -OutputMode single -Title "Please select a NAV Container" 
if ($Container -ne $null) {
    $Settings = "{ ""ContainerId"": ""$($Container.ID)"", ""ContainerName"": ""$($Container.Name)"", ""ClientPath"": $($Container.Path | ConvertTo-json) }"
    Set-Content -Value $Settings -Path (Join-Path $PSScriptRoot Settings.json)
}
else {
    Write-Host -ForegroundColor Red "No Container Selected"
    Start-Sleep -Seconds 2
    return
}
Write-Host -ForegroundColor Green "Done!"
Start-Sleep -Seconds 2