$Repo = Read-Host -Prompt "Enter Repository url"
if ($Repo -eq '') {
    return
}
git remote add origin $Repo
git push -u origin master