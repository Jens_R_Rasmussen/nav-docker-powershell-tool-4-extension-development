$Settings = Get-Content -Path (Join-Path $PSScriptRoot Settings.json) | ConvertFrom-Json
$Type = 'Codeunit', 'Table', 'Page', 'Report', 'Query', 'XmlPort' | Out-GridView -Title 'Select Object type' -PassThru
[void][Reflection.Assembly]::LoadWithPartialName('Microsoft.VisualBasic')
$title = $Type
$msg   = "$Type Id:"
$Id = [Microsoft.VisualBasic.Interaction]::InputBox($msg, $title)
$Filter = "Type=$Type;ID=$Id"
$CSideObjectPath = Join-Path (Get-Item $PSScriptRoot).Parent.FullName "CSide Convert"
New-Item -Path $CSideObjectPath -ItemType Directory -Force -ErrorAction Ignore | Out-Null
$CSideObjectPath = Join-Path $CSideObjectPath "$Type$Id.txt"

Start-Process -FilePath (Join-Path $Settings.ClientPath finsql.exe) -ArgumentList "command=ExportToNewSyntax, file=$CSideObjectPath, filter=$Filter, ServerName=$($Settings.ContainerName), Database=CronusDK, logfile=$(Join-Path $PSScriptRoot log.txt), ntauthentication=1, synchronizeschemachanges=force, navservername=$($Settings.ContainerName), navserverinstance=NAV, tenant=Default"
