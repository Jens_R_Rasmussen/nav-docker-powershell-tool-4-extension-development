if (-not ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {   
    $arguments = "& '" + $myinvocation.mycommand.definition + "'"
    Start-Process powershell -Verb runAs -ArgumentList $arguments
    Break
}
$Settings = Get-Content -Path (Join-Path $PSScriptRoot Settings.json) | ConvertFrom-Json
$Session = New-PSSession -ContainerId $Settings.ContainerId -RunAsAdministrator
$AppInfo = Invoke-Command -Session $Session -ScriptBlock {
    Import-Module 'C:\Program Files\Microsoft Dynamics NAV\*\Service\Microsoft.Dynamics.Nav.Apps.Management.dll' | Out-Null
    return Get-NAVAppInfo -ServerInstance NAV -Tenant Default
}

$App = $AppInfo | Select-Object AppId, Name, Publisher, Version, ExtensionType | Out-GridView -Title "Select App to unpublish" -OutputMode Single
if ($App -eq $null) {
    Remove-PSSession $Session
    Write-Host -ForegroundColor Red "No App Selected!"
    Start-Sleep -Seconds 2
    return
}

Write-Host -ForegroundColor Green "Removing: $($App.Name)"

Invoke-Command -Session $Session -ScriptBlock {
    param(
    $App
    )
    Import-Module 'C:\Program Files\Microsoft Dynamics NAV\*\Service\Microsoft.Dynamics.Nav.Apps.Management.dll' | Out-Null
    Uninstall-NAVApp -ServerInstance NAV -Name $App.Name -Version $App.Version
    Unpublish-NAVApp -ServerInstance NAV -Name $App.Name -Version $App.Version
    Sync-NAVApp -ServerInstance NAV -Tenant Default -Name $App.Name -mode Clean -Force
} -ArgumentList $App

Invoke-Command -Session $Session -ScriptBlock {
    Import-Module 'C:\Program Files\Microsoft Dynamics NAV\*\Service\NavAdminTool.ps1' | Out-Null
    $CurrentServerInstance = Get-NAVServerInstance -ServerInstance NAV
    $CurrentServerInstance | Set-NAVServerInstance -Restart -Verbose
}

Write-Host
Write-Host -ForegroundColor Green "Done!"

Start-Sleep -Seconds 2

Remove-PSSession $Session