if (-not ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {   
    $arguments = "& '" + $myinvocation.mycommand.definition + "'"
    Start-Process powershell -Verb runAs -ArgumentList $arguments
    Break
}
function Select-FileDialog
{
	param([string]$Title,[string]$Directory,[string]$Filter="All Files (*.*)|*.*")
	[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null
	$objForm = New-Object System.Windows.Forms.OpenFileDialog
	$objForm.InitialDirectory = $Directory
	$objForm.Filter = $Filter
	$objForm.Title = $Title
	$Show = $objForm.ShowDialog()
	If ($Show -eq "OK")
	{
		Return $objForm.FileName
	}
	Else
	{
		Write-Error "Operation cancelled by user."
	}
}

$Build = '2018-DK'
Write-Host -ForegroundColor Green "Run Docker Container: $Build"
$imageNameTag = "microsoft/dynamics-nav:$Build"

$UseWindowsClient = Read-Host -Prompt "Use Windows account (Y/N)"
if ($UseWindowsClient.ToUpper() -EQ 'Y')
{
    $WindowsCredentiel = Get-Credential -UserName $env:USERNAME -Message "Enter your password" -ErrorAction Stop
    if (!$WindowsCredentiel)
    {
        Write-Host -ForegroundColor Red "User canceled the script"
        exit;
    }
    $username = $WindowsCredentiel.UserName
    $BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($WindowsCredentiel.Password)
    $password = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)
    $authType = "Windows"
}
else
{
    $username = $env:USERNAME
    $SecurePassword = (Read-Host -Prompt "Enter your password" -AsSecureString)
    $BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($SecurePassword)
    $password = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)
    $authType = "NavUserPassword"
}
$locale = "da-DK"
$name = Read-Host -Prompt "Enter name of container:" 
$name = $name.ToUpper().Replace(".", "");

$licensefile = Select-FileDialog -Title "Select License file" -Directory "J:\NAV-Licenser\Udvikler\" -Filter "License files (*.flf)|*.flf" -ErrorAction Stop
if (!$licensefile)
{
    Write-Host -ForegroundColor Red "No License File Selected"
    exit;
}
$licensefileName = [System.IO.Path]::GetFileName($licensefile)
$ClientPath = "C:\Program Files (x86)\Microsoft Dynamics NAV\*\RoleTailored Client"
$MainPath = Join-Path $PSScriptRoot "NAV Client\$name"
Remove-Item -Path $MainPath -Confirm:$false
New-Item -Path $MainPath -ItemType Directory -Force -ErrorAction Ignore | Out-Null
Copy-Item -Path $licensefile -Destination (Join-Path $MainPath $licensefileName) -Force
New-Item -Path (Join-Path $MainPath "my") -ItemType Directory -Force -ErrorAction Ignore | Out-Null
New-Item -Path (Join-Path $MainPath "client") -ItemType Directory -Force -ErrorAction Ignore | Out-Null

docker ps -a --filter name=$name -q | ForEach-Object {
    Write-Host "Remove container $name"
    docker rm $_ -f | Out-Null
}

Write-Host "Pull new container image for $imageNameTag"
docker pull $imageNameTag

Write-Host "Run container"
$ContainerId = docker run -m 4G `
                        --name $name `
                        --hostname $name `
                        --env accept_eula=Y `
                        --env useSSL=N `
                        --env ClickOnce=Y `
                        --env auth=$authType `
                        --env username=$username `
                        --env password=$password `
                        --env ExitOnError=N `
                        --env licenseFile="C:\sharedfiles\$licensefileName" `
                        --detach `
                        --volume ${MainPath}:c:\sharedfiles `
                        --volume ${MainPath}\my:c:\run\my `
                        --volume ${MainPath}\client:c:\client `
                        --env locale=$locale `
                        $imageNameTag

if ($LastExitCode -ne 0) { throw "Docker run error" }

Write-Host "Waiting for container to be ready, this shouldn't take more than a few minutes"
Write-Host "Time:          ½              1              ½              2              ½              3              ½              4"
$cnt = 250
$log = ""
do {
    Write-Host -NoNewline "."
    Start-Sleep -Seconds 2
    $logs = docker logs $name
    if ($logs) { $log = [string]::Join(" ",$logs) }
    if ($log.Contains("<ScriptBlock>")) { $cnt = 0 }
} while ($cnt-- -gt 0 -and !($log.Contains("Ready for connections!")))

if ($cnt -gt 0) {
    Write-Host "Ready"
    
    Write-Host "Open Click Once Webpage"
    Start-Process -FilePath "http://$($name):8080/NAV"

    Write-Host "Start Web Client"
    Start-Process -FilePath "http://$name/NAV/WebClient"

    Write-Host "Copy new .vsix file to $MainPath"
    remove-item "$MainPath\*.vsix" -force
    docker exec -it $name powershell "Copy-item -Path 'C:\Run\*.vsix' -destination 'C:\sharedfiles' -force"

    Write-Host "Copy Client"
    remove-item "$MainPath\client\*" -force
    docker exec -it $name powershell "Copy-item -Path '$ClientPath\*' -destination 'C:\client' -Recurse -Force"
}
Write-Host "Container Output:"
docker logs $name | ForEach-Object { Write-Host $_ }
$Settings = "{ ""ContainerId"": ""$ContainerId"", ""ContainerName"": ""$name"", ""ClientPath"": $("$MainPath\client" | ConvertTo-json) }"
Set-Content -Value $Settings -Path (Join-Path $PSScriptRoot Settings.json)
Read-Host "Done!"