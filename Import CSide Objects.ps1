$Settings = Get-Content -Path (Join-Path $PSScriptRoot Settings.json) | ConvertFrom-Json
$CSideObjectPath = Join-Path (Get-Item $PSScriptRoot).Parent.FullName CSide
$ObjectFiles = Get-Item $CSideObjectPath\* -Filter "*.txt"
$ObjectFiles | ForEach-Object {
    Start-Process -FilePath (Join-Path $Settings.ClientPath finsql.exe) -ArgumentList "command=importobjects, file=$($_), ServerName=$($Settings.ContainerName), Database=CronusDK, logfile=$(Join-Path $PSScriptRoot log.txt), importaction=1, ntauthentication=1, synchronizeschemachanges=force, navservername=$($Settings.ContainerName), navserverinstance=NAV, tenant=Default"
}
