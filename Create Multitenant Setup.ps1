if (-not ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {   
    $arguments = "& '" + $myinvocation.mycommand.definition + "'"
    Start-Process powershell -Verb runAs -ArgumentList $arguments
    Break
}
$Settings = Get-Content -Path (Join-Path $PSScriptRoot Settings.json) | ConvertFrom-Json
$Session = New-PSSession -ContainerId $Settings.ContainerId -RunAsAdministrator
Invoke-Command -Session $Session -ScriptBlock {
    Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Force
    Import-Module 'C:\Program Files\Microsoft Dynamics NAV\*\Service\NavAdminTool.ps1' | Out-Null
    
    $ServerInstanceName = "NAV" 
    $DatabaseServer = "localhost"
    $DatabaseInstance = "sqlexpress"
    $Databasename = "CronusDK"
    $AppDatabase = "CronusDKApplication"
    $MainTenant = "Cockpit"
    
    $CurrentServerInstance = Get-NAVServerInstance -ServerInstance $ServerInstanceName
    
    Write-host "Split the Application Database and Customer Data"
    Export-NAVApplication -DatabaseServer $DatabaseServer -DatabaseInstance $DatabaseInstance -DatabaseName $Databasename -DestinationDatabaseName $AppDatabase -Force -Verbose
    Remove-NAVApplication -DatabaseServer $DatabaseServer -DatabaseInstance $DatabaseInstance -DatabaseName $Databasename -Force -Verbose
    
    Write-host "Prepare NST for MultiTenancy"
    $CurrentServerInstance | Set-NAVServerInstance -stop
    $CurrentServerInstance | Set-NAVServerConfiguration -KeyName MultiTenant -KeyValue "true"
    $CurrentServerInstance | Set-NAVServerConfiguration -KeyName DatabaseName -KeyValue ""
    $CurrentServerInstance | Set-NAVServerInstance -start
    
    Write-host "Mount app"
    $CurrentServerInstance | Mount-NAVApplication -DatabaseServer $DatabaseServer -DatabaseInstance $DatabaseInstance -DatabaseName $AppDatabase
    
    Write-host "Create Tenants"
    $CurrentServerInstance | Mount-NAVTenant -Id $MainTenant -DatabaseName $Databasename -AllowAppDatabaseWrite -OverwriteTenantIdInDatabase
}
Remove-PSSession $Session

Write-Host
Write-Host -ForegroundColor Green "Done!"
Start-Sleep -Seconds 2
