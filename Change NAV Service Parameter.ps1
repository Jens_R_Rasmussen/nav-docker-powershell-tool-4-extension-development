if (-not ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {   
    $arguments = "& '" + $myinvocation.mycommand.definition + "'"
    Start-Process powershell -Verb runAs -ArgumentList $arguments
    Break
}
$Settings = Get-Content -Path (Join-Path $PSScriptRoot Settings.json) | ConvertFrom-Json
$Session = New-PSSession -ContainerId $Settings.ContainerId -RunAsAdministrator
[xml]$Config = Invoke-Command -Session $Session -ScriptBlock {
    return Get-Content -Path 'C:\Program Files\microsoft dynamics nav\*\service\customsettings.config'
}
$Node = $Config.appSettings.add | Out-GridView -Title "Select Key to change" -OutputMode Single 
if ($Node -eq $null) {
    Write-Host
    Write-Host -ForegroundColor Red "No Key Selected"
    Start-Sleep -Seconds 2
    return
}
[void][Reflection.Assembly]::LoadWithPartialName('Microsoft.VisualBasic')
$Node.value = [Microsoft.VisualBasic.Interaction]::InputBox("Selected Key: $($Node.key) current Value: $($Node.Value)", "Type New Value")

Write-Host
Write-Host -ForegroundColor Green "Updating key: $($Node.key) to '$($Node.value)'"

Invoke-Command -Session $Session -ScriptBlock {
    param($Node)
    Import-Module 'C:\Program Files\Microsoft Dynamics NAV\*\Service\NavAdminTool.ps1' | Out-Null
    $CurrentServerInstance = Get-NAVServerInstance -ServerInstance NAV
    $CurrentServerInstance | Set-NAVServerConfiguration -KeyName $Node.key -KeyValue $Node.value
} -ArgumentList $Node
Remove-PSSession $Session

Write-Host
Write-Host -ForegroundColor Green "Done!"
Start-Sleep -Seconds 2

