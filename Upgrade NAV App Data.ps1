$Settings = Get-Content -Path (Join-Path $PSScriptRoot Settings.json) | ConvertFrom-Json
$Session = New-PSSession -ContainerId $Settings.ContainerId -RunAsAdministrator
Invoke-Command -Session $Session -ScriptBlock {
    Import-Module 'C:\Program Files\Microsoft Dynamics NAV\*\Service\NavAdminTool.ps1' | Out-Null
    $CurrentServerInstance = Get-NAVServerInstance -ServerInstance NAV
    $CurrentServerInstance | Set-NAVServerInstance -Start -Verbose
}
Remove-PSSession $Session

Write-Host
Write-Host -ForegroundColor Green "Done!"
Start-Sleep -Seconds 2



$Server = 'UDV-NAVSQL01'
$Instance = 'NPI-110-KNB'
$AppFile = Get-Item (Join-Path ((Get-Item -Path $PSScriptRoot).Parent).FullName '*.app')
$ContainerAppFile = (Join-Path "c:\temp" (Split-Path $AppFile -Leaf))

$Session = New-PSSession -ComputerName $Server -Credential $env:USERNAME
 
Invoke-Command -Session $Session -ScriptBlock {
    New-Item -Path "c:\temp" -ItemType Directory -Force -ErrorAction Ignore | Out-Null
}
Copy-Item -ToSession $Session -Path $AppFile -Destination $ContainerAppFile -Recurse -Force


Invoke-Command -Session $Session -ArgumentList $ContainerAppFile,$Instance -ScriptBlock {

    param($ContainerAppFile,$Instance)

    Import-Module 'C:\Program Files\Microsoft Dynamics NAV\*\Service\Microsoft.Dynamics.Nav.Apps.Management.dll'

    Publish-NAVApp -ServerInstance $Instance -Path $ContainerAppFile -SkipVerification

    Sync-NAVApp -ServerInstance $Instance -Path $ContainerAppFile -Tenant Default

    Install-NAVApp -ServerInstance $Instance -Path $ContainerAppFile -Tenant Default
}

Remove-PSSession $Session