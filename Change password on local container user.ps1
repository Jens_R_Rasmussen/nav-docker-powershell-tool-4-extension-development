﻿if (-not ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {   
    $arguments = "& '" + $myinvocation.mycommand.definition + "'"
    Start-Process powershell -Verb runAs -ArgumentList $arguments
    Break
}
$Settings = Get-Content -Path (Join-Path $PSScriptRoot Settings.json) | ConvertFrom-Json
$Session = New-PSSession -ContainerId $Settings.ContainerId -RunAsAdministrator
Invoke-Command -Session $Session -ArgumentList $env:USERNAME -ScriptBlock {
    param($UserName)
    $Password = Read-Host -AsSecureString
    $UserAccount = Get-LocalUser -Name $UserName
    $UserAccount | Set-LocalUser -Password $Password
}
Remove-PSSession $Session