if (-not ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {   
    $arguments = "& '" + $myinvocation.mycommand.definition + "'"
    Start-Process powershell -Verb runAs -ArgumentList $arguments
    Break
}
Clear-Host
$Object = @()
[switch]$StartContainer = $false
docker ps -a --no-trunc --format '{{.Names}};{{.ID}};{{.Status}};{{.Labels}};{{.Image}};{{.Mounts}}' | ForEach-Object { 
    $Items = $_.split(';');
    $Row = @{Name = "$($Items.Item(0))"; ID = "$($Items.Item(1))"; Status = "$($Items.Item(2))"; Image = "$($Items.Item(4))"}
    $Items.Item(3).split(',') | ForEach-Object {
        $Row.Add($_.Substring(0, 1).ToUpper() + $_.Substring(1, $_.IndexOf('=') - 1), $_.Substring($_.IndexOf('=') + 1))
    }
    $Items.Item(5).split(',') | ForEach-Object {
        if ($_ -like '*client')
        {    
            $Row.Add('Path', $_)
        }
    }
    $Object += $Row
}
$Object = $Object | ForEach-Object { New-Object object | Add-Member -NotePropertyMembers $_ -PassThru }

$RunningNavContainers = $Object | Where-Object {($_.Status -notlike "Exited*") -and ($_.Nav -ne $null)}
$StopedNavContainers = $Object | Where-Object {($_.Status -Like "Exited*") -and ($_.Nav -ne $null)}

if ($RunningNavContainers -eq $null -and $StopedNavContainers -eq $null) {
    Write-Host -ForegroundColor Red "There is no NAV Container on this machine"
    Read-Host
    return
}

if ($RunningNavContainers -ne $null -and $StopedNavContainers -ne $null) {
    $StartContainer = ((Read-Host -Prompt "Do you want to start a container? (Y/N)").ToUpper() -eq 'Y')
}
else {
    $StartContainer = ($StopedNavContainers.Count -ne 0)
}    

if ($StartContainer) {
    $Container = $StopedNavContainers | Select-Object Name, ID, Nav, Cu, Version, Path | Out-GridView -OutputMode single -Title "Please select a NAV Container to start" 
    if ($Container -ne $null) {
        Write-Host -ForegroundColor Green "Starting $($Container.Name) Container"
        docker start $Container.id
    }
}
else {
    $Container = $RunningNavContainers | Select-Object Name, ID, Nav, Cu, Version, Path | Out-GridView -OutputMode single -Title "Please select a NAV Container to stop" 
    if ($Container -ne $null) {
        Write-Host -ForegroundColor Green "Stopping $($Container.Name) Container"
        docker stop $Container.id
    }
}
Write-Host -ForegroundColor Green "Done!"
Start-Sleep -Seconds 2