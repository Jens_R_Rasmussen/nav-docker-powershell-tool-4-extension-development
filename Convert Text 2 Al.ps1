$Settings = Get-Content -Path (Join-Path $PSScriptRoot Settings.json) | ConvertFrom-Json
$CSideObjectPath = Join-Path (Get-Item $PSScriptRoot).Parent.FullName "CSide Convert"
$ConvertedObjectPath = Join-Path (Get-Item $PSScriptRoot).Parent.FullName "AL Converted"
$ObjectFiles = Get-Item $CSideObjectPath\* -Filter "*.txt"
$ObjectFiles | ForEach-Object {
    Start-Process -FilePath (Join-Path $Settings.ClientPath txt2al.exe) -ArgumentList "--source=""$CSideObjectPath"" --target=""$ConvertedObjectPath"" --rename --extensionStartId=50000"
}
