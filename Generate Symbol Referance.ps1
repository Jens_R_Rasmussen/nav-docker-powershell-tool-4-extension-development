if (-not ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {   
    $arguments = "& '" + $myinvocation.mycommand.definition + "'"
    Start-Process powershell -Verb runAs -ArgumentList $arguments
    Break
}
$Settings = Get-Content -Path (Join-Path $PSScriptRoot Settings.json) | ConvertFrom-Json
$Session = New-PSSession -ContainerId $Settings.ContainerId -RunAsAdministrator
Invoke-Command -Session $Session -ScriptBlock {
    [xml]$Config = Get-Content -Path 'C:\Program Files\microsoft dynamics nav\*\service\customsettings.config'
    $Node = $Config.appSettings.add | Where-Object key -EQ "EnableSymbolLoadingAtServerStartup"
    if ($Node.value.ToUpper() -eq "FALSE") {
        Import-Module 'C:\Program Files\Microsoft Dynamics NAV\*\Service\NavAdminTool.ps1' | Out-Null
        $CurrentServerInstance = Get-NAVServerInstance -ServerInstance NAV
        $CurrentServerInstance | Set-NAVServerInstance -stop
        $CurrentServerInstance | Set-NAVServerConfiguration -KeyName EnableSymbolLoadingAtServerStartup -KeyValue True
        $CurrentServerInstance | Set-NAVServerInstance -Start
    }
    $Process = Start-Process -PassThru -FilePath (Join-Path "C:\Program Files (x86)\Microsoft Dynamics NAV\*\RoleTailored Client" finsql.exe) -Verb runas -ArgumentList "command=generatesymbolreference, logfile=$(Join-Path "C:\sharedfiles" log.txt), Database=CronusDK, ServerName=Localhost, ntauthentication=1, navservername=Localhost, navserverinstance=NAV, tenant=Default"
    do {
        Write-Host -NoNewline "."
        Start-Sleep -Seconds 2
    } while ($Process.HasExited -eq $false)
}
Remove-PSSession $Session

Write-Host
Write-Host -ForegroundColor Green "Done!"
Start-Sleep -Seconds 2