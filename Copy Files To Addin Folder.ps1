if (-not ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {   
    $arguments = "& '" + $myinvocation.mycommand.definition + "'"
    Start-Process powershell -Verb runAs -ArgumentList $arguments
    Break
}
$Settings = Get-Content -Path (Join-Path $PSScriptRoot Settings.json) | ConvertFrom-Json
$Session = New-PSSession -ContainerId $Settings.ContainerId -RunAsAdministrator
$AddinsFolder = Invoke-Command -Session $Session -ScriptBlock {
    return Get-Item 'C:\Program Files\Microsoft Dynamics NAV\*\Service\Add-ins\'
}
Copy-Item -ToSession $Session -Destination $AddinsFolder -Path (Join-Path $PSScriptRoot "Add-ins") -Recurse
Remove-PSSession $Session

Write-Host
Write-Host -ForegroundColor Green "Done!"
Start-Sleep -Seconds 2